This addon copies links from the Web in HTML or Markdown formats.

## The Problem of Copying a Link

A link on the web usually consists of a URL it points to, and some
clickable text. HTML and Markdown users have to do multiple steps to
copy such a link to their writing:

  1. use context menu to copy the link location (URL),
  2. switch to their writing
  3. start writing the link code:

       * `<a href="` _paste the URL here_ `"></a>` (HTML users)
       * `[](` _paste the URL here_ `)` (Markdown users)

  4. switch back to the web page with the link
  5. select the clickable text (avoid unintended clicks)
  6. copy the clickable text
  7. switch back to their writing
  8. change cursor position and finish writing the link code:

      * `<a href="...">` _paste the text here_ `</a>` (HTML users)
      * `[` _paste the text here_ `](...)` (Markdown users)

## Using the Addon

This addon copies the entire link code, the URL and the clickable text
together. It creates two context menu items:

  * Copy HTML Link:

    save HTML code in the clipboard,
    e.g. `<a href="http://www.example.com">Example Domain</a>`.

  * Copy Markdown Link:

    save Markdown code in the clipboard,
    e.g. `[Example Domain](http://www.example.com")`.

Using this menu options instead of "Copy Link Location" reduces the
number of context switches necessary to copy a link (only once rather
than three times), and simplifies editing (no need to change cursor
position after pasting the link).
