var clipboard = require("sdk/clipboard");
var contextMenu = require("sdk/context-menu");
var _ = require("sdk/l10n").get;

var menuCopyHTMLItem = contextMenu.Item({
  label: _("copy_html_link_msg"),
  context: contextMenu.SelectorContext("a[href]"),
  contentScript: 'self.on("click", function (node, data) { ' +
         '  var text = node.textContent; ' +
         '  var href = node.href; ' +
         '  var htmlLink = "<a href=\\\"" + href + "\\\"/>" + text + "</a>"; ' +
         '  self.postMessage(htmlLink); ' +
         '});',
  onMessage: function(htmlLink) {
      clipboard.set(htmlLink);
  }
});

var menuCopyMDItem = contextMenu.Item({
  label: _("copy_md_link_msg"),
  context: contextMenu.SelectorContext("a[href]"),
  contentScript: 'self.on("click", function (node, data) { ' +
         '  var text = node.textContent; ' +
         '  var href = node.href; ' +
         '  var mdLink = "[" + text + "](" + href + ")"; ' +
         '  self.postMessage(mdLink); ' +
         '});',
  onMessage: function(mdLink) {
      clipboard.set(mdLink);
  }
});
